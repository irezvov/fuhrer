import Distribution.Simple
import Distribution.PackageDescription (emptyHookedBuildInfo)
import System.Process (system)

main = defaultMainWithHooks $ simpleUserHooks {
        preBuild = generateProto,
        postClean = cleanProtoFiles
    }

generateProto _ _ = do
  putStrLn "Generate ProtoBuf files"
  genProto "src" "king.proto"
  return emptyHookedBuildInfo

genProto outdir filename =
  system $ "hprotoc -I src -p Proto -d "
    ++ outdir ++ " " ++ filename

cleanProtoFiles _ _ _ _ = do
    system $ "rm -r src/Proto"
    return ()
