module Worker.Node.Income (
    income
) where

import Control.Applicative ((<$>))
import Control.Monad (forever)
import Control.Concurrent.Chan (Chan, writeChan)

import System.ZMQ3.Monadic
    (
        runZMQ, socket, liftIO,
        bind, receive, Pull(..)
    )

import Worker.Node.FSM
import Messages.Message
import Messages.Node
import Messages.Hello

income :: String -> Chan NodeEvent -> IO ()
income addr ch = runZMQ $ do
    s <- socket Pull
    bind s $ "tcp://" ++ addr
    forever $ do
        Just msg <- unpack <$> receive s
        liftIO $ writeChan ch $ msgToEvent msg

msgToEvent :: NodeMsg -> NodeEvent
msgToEvent (Hello (HelloMsg nd)) = HelloEvent nd
msgToEvent (Alive nid (AliveMsg mId)) = AliveEvent nid mId
msgToEvent (Fine (FineThanksMsg mId)) = FineEvent mId
msgToEvent (King nid (ImTheKingMsg _)) = KingEvent nid
msgToEvent (Ping nid (PingMsg mid)) = PingEvent mid nid
msgToEvent (Pong (PongMsg mid)) = PongEvent mid
