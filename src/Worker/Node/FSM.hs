module Worker.Node.FSM (
    NodeEvent(..), NodeState(..), initPing, PingState(..)
) where

import Worker.Types
import Messages.Node

data NodeEvent = HelloEvent NodeDesc
               | AliveEvent NodeId Int
               | FineEvent Int
               | FineTimeout Int
               | KingEvent NodeId
               | KingTimeout
               | PingTick
               | PingEvent Int NodeId
               | PongEvent Int
    deriving (Show, Eq)

data PingState = PS {
        psNodeId :: NodeId,
        psMsgId :: Int,
        psTry :: Int,
        psPong :: Bool
    } deriving (Eq, Show)

initPing :: NodeId -> Int -> PingState
initPing nid mid = PS nid mid 0 False

data NodeState = StartVoiting
                | WaitFine Int
                | WaitKing
                | PingKing PingState
                | KingNode
    deriving (Eq, Show)
