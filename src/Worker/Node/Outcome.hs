module Worker.Node.Outcome (
    outcome, OutAction(..)
) where

import Control.Applicative ((<$>))
import Control.Monad (foldM, when)
import Control.Concurrent.Chan (Chan, readChan)
import qualified Data.Map as Map
import Data.Maybe (isJust, fromJust)

import System.ZMQ3.Monadic
    (
        runZMQ, socket, connect, liftIO,
        send, close, Push(..), Socket, ZMQ
    )

import Worker.Types

import Messages.Message

data OutAction a = Send NodeId a
                 | SendToAll a
                 | AddNode NodeDesc

data OutState z a = OutState {
        osNodes :: Map.Map NodeId (Socket z Push),
        osChan :: Chan (OutAction a),
        osLog :: String -> IO ()
    }

connectTo :: NodeDesc -> ZMQ z (Socket z Push)
connectTo nd = do
    s <- socket Push
    connect s $ "tcp://" ++ ndAddress nd
    return s

insertNode :: Map.Map NodeId (Socket z Push) -> NodeDesc -> ZMQ z  (Map.Map NodeId (Socket z Push))
insertNode m nd = do
    let nid = ndNodeId nd
    let oldSock = Map.lookup nid m
    when (isJust oldSock) $ close (fromJust oldSock)
    (flip $ Map.insert nid) m <$> connectTo nd

initState :: (String -> IO ()) -> [NodeDesc] -> Chan (OutAction a) -> ZMQ z (OutState z a)
initState logString ns ch = do
    nodes <- foldM insertNode Map.empty ns
    return $ OutState nodes ch logString

addNode :: NodeDesc -> OutState z a -> ZMQ z (OutState z a)
addNode nd st = do
    newNodes <- insertNode (osNodes st) nd
    return $ st { osNodes = newNodes }

sendMsg :: (Message a) => OutState z a -> NodeId -> a -> ZMQ z ()
sendMsg st to msg = do
    let sock = Map.lookup to $ osNodes st
    when (isJust sock) $ do
        liftIO $ osLog st $ "Send msg(" ++ (show msg) ++ ") to " ++ (show to)
        send (fromJust sock) [] $ pack msg

sendToAll :: (Message a) => (OutState z a) -> a -> ZMQ z ()
sendToAll st msg = do
    let socks = map snd $ Map.toList $ osNodes st
    liftIO $ osLog st $ "Send msg(" ++ (show msg) ++ ") to all"
    mapM_ (\s -> send s [] $ pack msg) socks

outcome :: Message a => (String -> IO ()) -> [NodeDesc] -> Chan (OutAction a) -> IO ()
outcome logString nodeDesc ch = runZMQ $ initState logString nodeDesc ch >>= loop
    where
        loop st = do
            cmd <- liftIO $ readChan $ osChan st
            newSt <- case cmd of
                (AddNode nd) -> addNode nd st
                (Send to msg) -> sendMsg st to msg >> return st
                (SendToAll msg) -> sendToAll st msg >> return st
            loop newSt

