module Main where

import Worker.Config (getConfig)
import Worker.Node (runNode)

main :: IO ()
main = do
    cfg <- getConfig
    runNode cfg
