module Worker.Node (runNode) where

import Control.Applicative ((<$>), (<*>))
import Control.Monad (when, unless)
import Data.List (delete)

import System.ZMQ3.Monadic
    (
        runZMQ, socket, connect,
        send, receive, Req(..)
    )

import Worker.Types
import Worker.Utils
import Worker.Config
import qualified Worker.Logger as Log
import Worker.Monad
import Worker.Node.FSM

import Messages.Message
import Messages.Hello
import Messages.HelloRes
import Messages.Node

lookupNodes :: NodeId -> String -> String -> Int -> IO (Maybe HelloResMsg)
lookupNodes nid nodeAddr addr port = runZMQ $ do
    s <- socket Req
    connect s $ "tcp://" ++ addr ++ ":" ++ (show port)
    send s [] $ pack $ HelloMsg $ NodeDesc nid nodeAddr
    unpack <$> receive s

nodeAddress :: Config -> IO String
nodeAddress _cfg = do
    (nodeAddr, nodePort) <- (,) <$> getAddress <*> getFreePort
    return $ nodeAddr ++ ":" ++ (show nodePort)

runNode :: Config -> IO ()
runNode cfg = do
    let (addr, namePort) = (cfgLoggerAddr cfg, cfgNamePort cfg)
    let nid = cfgNodeId cfg
    fullAddr <- nodeAddress cfg
    Just (HelloResMsg pingT nodes) <- lookupNodes nid fullAddr addr namePort
    l <- Log.getLogger nid addr $ cfgLoggerPort cfg
    let log = Log.log l
    log $ (show nid) ++ " started at " ++ fullAddr
    runNodeMonad node pingT nid log nodes fullAddr

node :: Node ()
node = do
    sendHello
    startVoiting
    nodeLoop

nodeLoop :: Node ()
nodeLoop = do
    event <- waitNodeEvent
    st <- state
    handleEvent st event
    nodeLoop

handleHello :: NodeDesc -> Node ()
handleHello nd = do
    logString $ "Add node " ++ (show $ ndNodeId nd)
    addNode nd

startVoiting :: Node ()
startVoiting = do
    nodes <- majorNodes
    when (length nodes > 0) $ do
        logString "Start voiting"
        mid <- sendAlive nodes
        setState $ WaitFine mid
        delayEvent $ FineTimeout mid
    unless (length nodes > 0) $ do
        sendImTheKing
        setState KingNode

handleAlive :: Int -> NodeId -> Node ()
handleAlive mid nid = do
    logString $ "Receive ALIVE? from " ++ (show nid)
    sendFineThanks mid nid

waitKing :: Node ()
waitKing = do
    logString "Wait for King"
    setState WaitKing
    delayEvent KingTimeout

handleFineTimeout :: Node ()
handleFineTimeout = do
    logString $ "FineTimeout"
    sendImTheKing
    setState KingNode

handleKing :: NodeId -> Node ()
handleKing nid = do
    logString $ "Le Roi est mort, vive le Roi! (" ++ (show nid) ++ ")"
    startPingTimer
    mid <- sendPing nid
    setState $ PingKing $ initPing nid mid

handleKingIfBigger :: NodeId -> Node ()
handleKingIfBigger nid = do
    nid' <- ndNodeId <$> self
    when (nid > nid') $ handleKing nid

handlePingTimerTick :: PingState -> Node ()
handlePingTimerTick st@(PS { psTry = try }) = do
    when (psPong st) $ do
        mid <- sendPing $ psNodeId st
        setState $ PingKing $ st { psPong = False, psMsgId = mid }
    unless (psPong st) $ do
        when (try < 3) $ setState $ PingKing $ st { psTry = try + 1 }
        when (try == 3) $ stopPingTimer >> startVoiting

handlePong :: Int -> PingState -> Node ()
handlePong mid (st@PS { psNodeId = nid, psMsgId = mid'})
    | mid == mid' = setState $ PingKing $ st { psPong = True }
    | otherwise = return ()

-- node FSM transition
handleEvent :: NodeState  ->  NodeEvent -> Node ()
handleEvent   (WaitFine mid) (FineEvent mid')     | mid == mid' = waitKing
handleEvent   (WaitFine mid) (FineTimeout mid')   | mid == mid' = handleFineTimeout
handleEvent   (WaitFine _  ) (AliveEvent nid mId) = handleAlive mId nid
handleEvent   (WaitFine _  ) (KingEvent nid)      = handleKingIfBigger nid
handleEvent    WaitKing      (KingEvent nid)      = handleKing nid
handleEvent    WaitKing       KingTimeout         = logString "KingTimeout" >> startVoiting
handleEvent    WaitKing      (AliveEvent nid mId) = handleAlive mId nid
handleEvent   (PingKing _  ) (KingEvent nid')     = stopPingTimer >> handleKing nid'
handleEvent   (PingKing st )  PingTick            = handlePingTimerTick st
handleEvent   (PingKing st ) (PongEvent mid)      = handlePong mid st
handleEvent    KingNode      (PingEvent mid nid)  = sendPong mid nid
handleEvent    KingNode      (KingEvent nid)      = handleKingIfBigger nid
handleEvent    _             (AliveEvent nid mId) = handleAlive mId nid >> startVoiting
handleEvent    _             (HelloEvent nd)      = handleHello nd
handleEvent    _              event               = return ()

