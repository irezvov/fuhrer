module Worker.Config
       (
         Config(..),
         getConfig,
         defaultConfig
       )
       where

import System.Console.GetOpt
import System.Environment (getArgs)
import Data.Maybe (fromMaybe)

import Worker.Types

defaultConfig :: Config
defaultConfig = Config {
    cfgLoggerAddr = "127.0.0.1",
    cfgLoggerPort = 1844,
    cfgNamePort = 1448
}

options :: [OptDescr (Config -> Config)]
options =
    [ Option [] ["logaddr"]
        (OptArg ((\f cfg -> cfg { cfgLoggerAddr = f }) . fromMaybe "localhost") "ADDR")
        "logger address (default: localhost)"
    , Option [] ["logport"]
        (OptArg ((\f cfg -> cfg { cfgLoggerPort = read f }) . fromMaybe "1844" ) "PORT")
        "logger port (default: 1844)"
    , Option [] ["nameport"]
        (OptArg ((\f cfg -> cfg { cfgNamePort = read f }) . fromMaybe "1448" ) "PORT")
        "name node port (default: 1448)"
    , Option ['i'] ["id"]
         (ReqArg (\i cfg -> cfg { cfgNodeId = NodeId $ read i }) "NodeID")
         "Node ID"
    ]

getConfig :: IO Config
getConfig = getArgs >>= parseConfig

parseConfig :: [String] -> IO Config
parseConfig argv =
    case getOpt Permute options argv of
        (cfg, _rest, []) -> return $ foldl (flip id) defaultConfig cfg
        (_, _, err) -> ioError $ userError $ concat err ++ usageInfo "Usage:" options
