module Worker.Types where

data NodeDesc = NodeDesc { ndNodeId :: NodeId, ndAddress :: String } deriving (Eq)

instance Show NodeDesc where
    show (NodeDesc nid addr) = (show nid) ++ ":" ++ addr

newtype NodeId = NodeId { unNodeId :: Int } deriving (Eq, Ord)

instance Show NodeId where
    show (NodeId nid) = "Node#" ++ (show nid)

data Config = Config {
    cfgLoggerAddr :: String,
    cfgLoggerPort :: Int,
    cfgNamePort :: Int,
    cfgNodeId :: NodeId
} deriving (Show)