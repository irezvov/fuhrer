module Worker.Logger (
    getLogger, log, Logger
)
where

import Prelude hiding (log)
import Control.Concurrent (forkIO)
import Control.Concurrent.Chan (Chan, newChan, readChan, writeChan)
import Control.Monad (forever)

import qualified Data.ByteString as BS

import Data.Time.LocalTime (getZonedTime)
import Data.Time.Format (formatTime)
import System.Locale (defaultTimeLocale)

import System.ZMQ3.Monadic hiding (message)

import Worker.Types (NodeId(..))
import Messages.Message
import Messages.Log

data Logger = Logger {
    lgNodeId :: NodeId,
    lgReadQueue :: (Chan BS.ByteString)
}

type Address = String
type Port = Int

getLogger :: NodeId -> Address -> Port -> IO Logger
getLogger nid addr port = do
    queue <- liftIO newChan
    forkIO $ runZMQ $ do
        s <- socket Push
        connect s $ "tcp://" ++ addr ++ ":" ++ (show port)
        forever $ do
            msg <- liftIO $ readChan queue
            send s [] msg
    return $ Logger nid queue

log :: Logger -> String -> IO ()
log (Logger { lgReadQueue = q, lgNodeId = nid }) msg = do
    time <- getZonedTime >>= return . (formatTime defaultTimeLocale "%T%Q")
    writeChan q . pack $ LogMsg nid $ time ++ ": " ++ msg
