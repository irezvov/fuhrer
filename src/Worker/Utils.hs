module Worker.Utils (getFreePort, getAddress) where

import Network.Socket
import Network.Info
import Control.Applicative

getFreePort :: IO PortNumber
getFreePort = do
    let hints = defaultHints { addrFamily = AF_INET }
    addr <- head <$> getAddrInfo (Just hints) (Just "localhost") Nothing
    s <- socket (addrFamily addr) Stream defaultProtocol
    bind s $ addrAddress addr
    port <- socketPort s
    sClose s
    return port

getAddress :: IO String
getAddress =
    head . map (show . ipv4) . filter ((/= "lo") . name) <$> getNetworkInterfaces
