module Worker.Monad (
    Node, runNodeMonad,
    logString, msgCounter, addNode, delayEvent,
    sendPing, sendPong, startPingTimer, stopPingTimer,
    sendHello, sendFineThanks, sendAlive, majorNodes,
    sendImTheKing, waitNodeEvent, self, state, setState
) where

import Control.Monad.State hiding (state)
import Control.Applicative ((<$>))
import Data.Maybe (fromMaybe, isJust, fromJust)
import Data.Function (on)

import Control.Concurrent.Chan (Chan, newChan, readChan, writeChan)
import Control.Concurrent (forkIO, killThread, threadDelay, ThreadId)

import Worker.Types
import qualified Worker.Node.FSM as FSM
import Worker.Node.Outcome
import Worker.Node.Income

import Messages.Hello
import Messages.Node

data NodeState = NodeState {
        nsIn :: Chan FSM.NodeEvent,
        nsOut :: Chan (OutAction NodeMsg),
        nsSelf :: NodeDesc,
        nsMsgCounter :: Int,
        nsPingInt :: Int,
        nsLog :: String -> IO (),
        nsNodes :: [NodeDesc],
        nsState :: FSM.NodeState,
        nsPingTimer :: Maybe ThreadId
    }

type Node a = StateT NodeState IO a

runNodeMonad :: Node a -> Int -> NodeId -> (String -> IO ()) -> [NodeDesc] -> String -> IO a
runNodeMonad action pingInterval nid logStr nodes addr = do
    st <- initNodeState pingInterval nid logStr nodes addr
    evalStateT action st

initNodeState :: Int -> NodeId -> (String -> IO ()) -> [NodeDesc] -> String -> IO NodeState
initNodeState pingInterval nid logStr nodes addr = do
    inChan <- newChan
    outChan <- newChan
    forkIO $ outcome logStr nodes outChan
    forkIO $ income addr inChan
    return $ NodeState {
        nsIn = inChan,
        nsOut = outChan,
        nsSelf = NodeDesc nid addr,
        nsMsgCounter = 0,
        nsPingInt = pingInterval,
        nsLog = logStr,
        nsNodes = nodes,
        nsState = FSM.StartVoiting,
        nsPingTimer = Nothing
    }

waitNodeEvent :: Node FSM.NodeEvent
waitNodeEvent = do
    inChan <- gets nsIn
    liftIO $ readChan inChan

outAction :: OutAction NodeMsg -> Node ()
outAction msg = do
    out <- gets nsOut
    liftIO $ writeChan out $ msg

sendHello :: Node ()
sendHello =
    gets nsSelf >>= (outAction . SendToAll . Hello . HelloMsg)

sendAlive :: [NodeId] -> Node Int
sendAlive nodes = do
    self <- gets nsSelf
    mId <- msgCounter
    let msg = Alive (ndNodeId self) $ AliveMsg mId
    forM_ nodes $ outAction . (flip Send msg)
    return mId

sendFineThanks :: Int -> NodeId -> Node ()
sendFineThanks msgId nid =
    outAction $ Send nid $ Fine $ FineThanksMsg msgId

sendImTheKing :: Node ()
sendImTheKing = do
    self <- ndNodeId <$> gets nsSelf
    outAction $ SendToAll $ King self $ ImTheKingMsg Nothing

sendPing :: NodeId -> Node Int
sendPing nid = do
    self <- ndNodeId <$> gets nsSelf
    mId <- msgCounter
    outAction $ Send nid $ Ping self $ PingMsg mId
    return mId

sendPong :: Int -> NodeId -> Node ()
sendPong mid nid =
    outAction $ Send nid $ Pong $ PongMsg mid

state :: Node FSM.NodeState
state = gets nsState

setState :: FSM.NodeState -> Node ()
setState ns = do
    st <- get
    put $ st { nsState = ns }

logString :: String -> Node ()
logString msg = do
    l <- gets nsLog
    liftIO $ l msg

msgCounter :: Node Int
msgCounter = do
    st <- get
    put $ st { nsMsgCounter = nsMsgCounter st + 1 }
    return $ nsMsgCounter st

self :: Node NodeDesc
self = gets nsSelf

addNode :: NodeDesc -> Node ()
addNode nd = do
    st <- get
    when (nd `notElem` (nsNodes st)) $ do
        outAction $ AddNode nd
        let nodes = filter (on (/=) ndNodeId nd) $ nsNodes st
        put $ st { nsNodes = nd : nodes }

majorNodes :: Node [NodeId]
majorNodes = do
    self <- ndNodeId <$> gets nsSelf
    filter (> self) . map ndNodeId <$> gets nsNodes

delayEventAt :: Int -> FSM.NodeEvent -> Node ()
delayEventAt sec event = do
    inChan <- gets nsIn
    liftIO $ forkIO $ do
        threadDelay $ sec*1000*1000
        writeChan inChan event
    return ()

delayEvent :: FSM.NodeEvent -> Node ()
delayEvent event = do
    t <- gets nsPingInt
    delayEventAt t event

startPingTimer :: Node ()
startPingTimer = do
    st <- get
    inChan <- gets nsIn
    stopPingTimer
    thId <- liftIO $ forkIO $ forever $ do
        threadDelay $ (nsPingInt st)*1000*1000
        writeChan inChan FSM.PingTick
    put $ st { nsPingTimer = Just thId }

stopPingTimer :: Node ()
stopPingTimer = do
    st <- get
    when (isJust $ nsPingTimer st) $ liftIO $ killThread $ fromJust $ nsPingTimer st
    put $ st { nsPingTimer = Nothing }
