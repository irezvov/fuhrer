module Logger.Config (
    Config(..),
    getConfig,
    defaultConfig
)
where

import System.Console.GetOpt
import System.Environment (getArgs)
import Data.Maybe (fromMaybe)

data Config = Config {
    cfgLoggerPort :: Int,
    cfgNamePort :: Int
} deriving (Show)

defaultConfig :: Config
defaultConfig = Config {
    cfgLoggerPort = 1844,
    cfgNamePort = 1448
}

options :: [OptDescr (Config -> Config)]
options =
    [
      Option [] ["logport"]
        (OptArg ((\f cfg -> cfg { cfgLoggerPort = read f }) . fromMaybe "1844" ) "PORT")
        "logger port (default: 1844)"
    , Option [] ["nameport"]
        (OptArg ((\f cfg -> cfg { cfgNamePort = read f }) . fromMaybe "1448" ) "PORT")
        "name node port (default: 1448)"
    ]

getConfig :: IO Config
getConfig = getArgs >>= parseConfig

parseConfig :: [String] -> IO Config
parseConfig argv =
    case getOpt Permute options argv of
        (cfg, _rest, []) -> return $ foldl (flip id) defaultConfig cfg
        (_, _, err) -> ioError $ userError $ concat err ++ usageInfo "Usage:" options


