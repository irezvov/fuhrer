module Main where

import Control.Concurrent (forkIO)

import Logger.Config
import Logger.LogServer (logServer)
import Logger.NameServer (nameServer)

main :: IO ()
main = do
    cfg <- getConfig
    forkIO $ nameServer $ show $ cfgNamePort cfg
    logServer $ show $ cfgLoggerPort cfg
