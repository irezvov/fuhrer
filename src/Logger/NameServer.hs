module Logger.NameServer (nameServer) where

import Control.Applicative ((<$>))
import Data.Function (on)
import qualified Data.ByteString as BS

import System.ZMQ3.Monadic (
    socket, bind, receive, runZMQ, send,
    liftIO, Rep(..)
    )

import Messages.Message
import Messages.Hello
import Messages.HelloRes
import Worker.Types

type State = [NodeDesc]

handleHelloMessage :: Int -> State -> BS.ByteString -> (BS.ByteString, State)
handleHelloMessage pingInterval nodes msg =
    let Just (HelloMsg node) = unpack msg
        nodes' = filter (on (/=) ndNodeId node) nodes
    in (pack $ HelloResMsg pingInterval nodes', node : nodes')

nameServer :: String -> IO ()
nameServer port = runZMQ $ do
    s <- socket Rep
    bind s $ "tcp://*:" ++ port
    liftIO $ putStrLn $ "Name server start at " ++ port
    loop s []
  where
    loop s nodes = do
        (resp, newNodes) <- handleHelloMessage 3 nodes <$> receive s
        send s [] resp
        loop s newNodes
