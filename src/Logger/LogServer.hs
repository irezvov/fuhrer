module Logger.LogServer (logServer) where

import Control.Monad (forever)
import Data.Maybe (fromJust)
import qualified Data.ByteString as BS

import System.ZMQ3.Monadic (
    socket, bind, receive, runZMQ,
    liftIO, Pull(..)
    )

import Messages.Message
import Messages.Log

printLogMsg :: LogMsg -> IO ()
printLogMsg (LogMsg{ lmNodeId = nid, lmMessage = msg }) =
    putStrLn $ (show nid) ++ ": " ++ msg

handleMessage :: BS.ByteString -> IO ()
handleMessage = printLogMsg . fromJust . unpack

logServer :: String -> IO ()
logServer port = runZMQ $ do
    s <- socket Pull
    bind s $ "tcp://*:" ++ port
    liftIO $ putStrLn $ "Log server is started at " ++ port
    forever $ receive s >>= liftIO . handleMessage
