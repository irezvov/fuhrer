module Messages.Log (LogMsg(..)) where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as Lazy

import Text.ProtocolBuffers.Basic (uFromString, uToString, defaultValue)
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Messages.Message
import Worker.Types

import Proto.King.Log

data LogMsg = LogMsg {
        lmNodeId :: NodeId,
        lmMessage :: String
    } deriving (Show)

defaultLog :: Log
defaultLog = defaultValue

packLogMsg :: LogMsg -> BS.ByteString
packLogMsg (LogMsg n msg) = Lazy.toStrict $ messagePut $ defaultLog {
    lNodeId = fromIntegral $ unNodeId n,
    lMessage = uFromString msg
    }

instance Message LogMsg where
    pack = packLogMsg
    unpack binary = case messageGet $ Lazy.fromStrict binary of
        Left _ -> Nothing
        Right (logMsg, _rest) ->
            let nid = NodeId $ fromIntegral $ lNodeId logMsg
                addr = uToString $ lMessage logMsg
            in Just $ LogMsg nid addr
