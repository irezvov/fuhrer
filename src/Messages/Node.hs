module Messages.Node (
    NodeMsg(..), AliveMsg(..), FineThanksMsg(..),
    ImTheKingMsg(..), PingMsg(..), PongMsg(..)
) where

import Data.Maybe (fromJust)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as Lazy

import Text.ProtocolBuffers.Basic (defaultValue)
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Messages.Message
import Messages.Hello
import Worker.Types

import qualified Proto.King.NodeMsg as NM
import Proto.King.NodeMsg.NodeMsgType

import qualified Proto.King.Alive as Alive
import qualified Proto.King.FineThanks as Fine
import qualified Proto.King.ImTheKing as King
import qualified Proto.King.Ping as Ping
import qualified Proto.King.Pong as Pong

data AliveMsg = AliveMsg {
        amMsgId :: Int
    } deriving (Show, Eq)

eitherToJust :: Either a (b, c) -> Maybe b
eitherToJust (Left _) = Nothing
eitherToJust (Right (msg, _rest)) = Just msg

instance Message AliveMsg where
    pack (AliveMsg mId) =
        Lazy.toStrict $ messagePut $ defaultValue { Alive.msgId = fromIntegral mId }
    unpack = (fmap $ AliveMsg . fromIntegral . Alive.msgId)
        . eitherToJust . messageGet . Lazy.fromStrict

data FineThanksMsg = FineThanksMsg {
        ftmMsgId :: Int
    } deriving (Show, Eq)

instance Message FineThanksMsg where
    pack (FineThanksMsg mId) =
        Lazy.toStrict $ messagePut $ defaultValue { Fine.msgId = fromIntegral mId }
    unpack = (fmap $ FineThanksMsg . fromIntegral . Fine.msgId)
        . eitherToJust . messageGet . Lazy.fromStrict

data ImTheKingMsg = ImTheKingMsg {
        itkmMsgId :: Maybe Int
    } deriving (Show, Eq)

instance Message ImTheKingMsg where
    pack (ImTheKingMsg mId) = Lazy.toStrict $ messagePut $ defaultValue {
            King.msgId = fmap fromIntegral $ mId
        }
    unpack = (fmap $ ImTheKingMsg . fmap fromIntegral . King.msgId)
        . eitherToJust . messageGet . Lazy.fromStrict

data PingMsg = PingMsg {
        pimMsgId :: Int
    } deriving (Show, Eq)

instance Message PingMsg where
    pack (PingMsg mId) =
        Lazy.toStrict $ messagePut $ defaultValue { Ping.msgId = fromIntegral mId }
    unpack = (fmap $ PingMsg . fromIntegral . Ping.msgId)
        . eitherToJust . messageGet . Lazy.fromStrict

data PongMsg = PongMsg {
        pomMsgId :: Int
    } deriving (Show, Eq)

instance Message PongMsg where
    pack (PongMsg mId) =
        Lazy.toStrict $ messagePut $ defaultValue { Pong.msgId = fromIntegral mId }
    unpack = (fmap $ PongMsg . fromIntegral . Pong.msgId)
        . eitherToJust . messageGet . Lazy.fromStrict

data NodeMsg = Hello HelloMsg
             | Alive NodeId AliveMsg
             | Fine FineThanksMsg
             | King NodeId ImTheKingMsg
             | Ping NodeId PingMsg
             | Pong PongMsg
    deriving (Eq)

instance Show NodeMsg where
    show (Hello (HelloMsg nd)) = "HELLO from " ++ (show $ ndNodeId nd)
    show (Alive _ (AliveMsg mid)) =
        "{" ++ (show mid) ++ "} ALIVE?"
    show (Fine (FineThanksMsg mid)) = "{" ++ (show mid) ++ "} FINETHANKS"
    show (King _ (ImTheKingMsg _)) = "IMTHEKING"
    show (Ping nid (PingMsg mid)) = "{" ++ (show mid) ++ "} PING"
    show (Pong (PongMsg mid)) = "{" ++ (show mid) ++ "} PONG"

instance Message NodeMsg where
    pack (Hello    m) = packNodeMsg HELLO Nothing $ pack m
    pack (Alive nd m) = packNodeMsg ALIVE (Just nd) $ pack m
    pack (Fine     m) = packNodeMsg FINETHANKS Nothing $ pack m
    pack (King nd  m) = packNodeMsg IMTHEKING (Just nd) $ pack m
    pack (Ping nd  m) = packNodeMsg PING (Just nd) $ pack m
    pack (Pong     m) = packNodeMsg PONG Nothing $ pack m

    unpack = unpackNodeMsg

packNodeMsg :: NodeMsgType -> Maybe NodeId -> BS.ByteString -> BS.ByteString
packNodeMsg msgType nd body = Lazy.toStrict $ messagePut $ defaultValue {
        NM.nmType = msgType,
        NM.nmBody = Lazy.fromStrict body,
        NM.nmFrom = fmap (fromIntegral . unNodeId) nd
    }

unpackNodeMsg :: BS.ByteString -> Maybe NodeMsg
unpackNodeMsg binary = case messageGet $ Lazy.fromStrict binary of
    Left _ -> Nothing
    Right (nodeMsg, _rest) ->
        let body = Lazy.toStrict $ NM.nmBody nodeMsg
            from = fmap (NodeId . fromIntegral) $ NM.nmFrom nodeMsg
            toMsgFrom ctr = fmap (ctr $ fromJust from) $ unpack body
            toMsg ctr = fmap ctr $ unpack body
        in case NM.nmType nodeMsg of
            HELLO      -> toMsg Hello
            ALIVE      -> toMsgFrom Alive
            FINETHANKS -> toMsg Fine
            IMTHEKING  -> toMsgFrom King
            PING       -> toMsgFrom Ping
            PONG       -> toMsg Pong

