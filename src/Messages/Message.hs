module Messages.Message where

import qualified Data.ByteString as BS

class (Show a) => Message a where
    pack :: a -> BS.ByteString
    unpack :: BS.ByteString -> Maybe a


