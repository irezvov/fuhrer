module Messages.Hello (HelloMsg(..)) where

import qualified Data.ByteString.Lazy as Lazy

import Text.ProtocolBuffers.Basic (uFromString, uToString, defaultValue)
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Messages.Message
import Worker.Types

import Proto.King.Hello
import Proto.King.NodeDescription

data HelloMsg = HelloMsg NodeDesc deriving (Show, Eq)

defaultHello :: Hello
defaultHello = defaultValue

instance Message HelloMsg where
    pack (HelloMsg (NodeDesc nid addr)) =
        Lazy.toStrict $ messagePut $ defaultHello { 
            hNodeDesc = NodeDescription (fromIntegral $ unNodeId nid) $ uFromString addr
        }
    unpack binary = case messageGet $ Lazy.fromStrict binary of
        Left _ -> Nothing
        Right (hello, _rest) ->
            let nd = hNodeDesc hello
                nid = NodeId $ fromIntegral $ ndId nd
                addr = uToString $ ndAddr nd
            in Just $ HelloMsg $ NodeDesc nid addr
