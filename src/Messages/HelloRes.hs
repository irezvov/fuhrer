module Messages.HelloRes (HelloResMsg(..), fromNodeDesc, toNodeDesc) where

import Data.Foldable (toList)
import Data.Sequence (fromList)

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as Lazy

import Text.ProtocolBuffers.Basic (uFromString, uToString, defaultValue)
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Worker.Types
import Messages.Message

import Proto.King.HelloRes
import Proto.King.NodeDescription

data HelloResMsg = HelloResMsg {
        hrmPingInterval :: Int,
        hrmNodes :: [NodeDesc]
    } deriving (Show)

defaultHelloRes :: HelloRes
defaultHelloRes = defaultValue

toNodeDesc :: NodeDescription -> NodeDesc
toNodeDesc (NodeDescription { ndId = nid, ndAddr = addr }) =
    NodeDesc (NodeId $ fromIntegral nid) (uToString addr)

unpackHelloRes :: BS.ByteString -> Maybe HelloResMsg
unpackHelloRes msg = case messageGet $ Lazy.fromStrict msg of
    Left _ -> Nothing
    Right (helloRes, _rest) ->
        let nodes = map toNodeDesc $ toList $ hrNodes helloRes
            pingInt = fromIntegral $ hrPingInterval helloRes
        in Just $ HelloResMsg pingInt nodes

fromNodeDesc :: NodeDesc -> NodeDescription
fromNodeDesc (NodeDesc nid addr) =
    NodeDescription (fromIntegral $ unNodeId nid) (uFromString addr)

instance Message HelloResMsg where
    pack (HelloResMsg ping nodes) = Lazy.toStrict $ messagePut $ defaultHelloRes {
            hrPingInterval = fromIntegral ping,
            hrNodes = fromList $ map fromNodeDesc $ nodes
        }
    unpack = unpackHelloRes


